define([
  'Leaflet',
  'jquery'
], function(Leaflet, jquery) {

  //make a map, set the center
  var map = Leaflet.map('map');
  map.setView([38.2500, -85.7667], 13);

  //OSM data can be accessed as tiles (x/y/z)
  var osmLayer = Leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  osmLayer.addTo(map);

  //many other data sources can be accessed as (x/y/z) tiles as well
  //e.g. an arcgis layer
  var arcgisLayer = Leaflet.tileLayer('http://a.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png');
  arcgisLayer.addTo(map);

  //we can also include vector data. geojson is a very popular format.
  var nbLayer;
  jquery
    .ajax({
      url: 'louisville_neighbourhoods.geojson'
    })
    .then(function(nbData) {
      //we can interact with vector data
      nbLayer = Leaflet.geoJson(nbData, {
        onEachFeature: function(feature, layer) {
          //popups are fully managed by leaflet
          layer.bindPopup(feature.properties.name);
        }
      });
      nbLayer.addTo(map);
    })
    .then(function() {

      //use a layer control to turn on and off layers

      //base maps are mutually exclusive
      var baseMaps = {
        "Open Street Maps": osmLayer,
        "Bicycle Map": arcgisLayer
      };

      //overlay maps are not.
      var overlayMaps = {
        "Neighbourhoods": nbLayer
      };

      Leaflet.control.layers(baseMaps, overlayMaps).addTo(map);
    });

});