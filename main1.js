define([
  'Leaflet',
  'jquery'
], function(Leaflet, jquery) {

  //make a map, set the center
  var map = Leaflet.map('map');
  map.setView([38.2500, -85.7667], 13);

  //OSM data can be accessed as tiles (x/y/z)
  var osmLayer = Leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  osmLayer.addTo(map);

  //many other data sources can be accessed as (x/y/z) tiles as well
  //e.g. an arcgis layer
  var arcgisLayer = Leaflet.tileLayer('http://a.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png');
  arcgisLayer.addTo(map);

});