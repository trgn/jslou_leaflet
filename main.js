define([
  'Leaflet',
  'jquery'
], function(Leaflet, jquery) {

  //make a map, set the center
  var map = Leaflet.map('map');
  map.setView([38.2500, -85.7667], 13);

  //OSM data can be accessed as tiles (x/y/z)
  var osmLayer = Leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  osmLayer.addTo(map);

  //many other data sources can be accessed as (x/y/z) tiles as well
  //e.g. an arcgis layer
  var arcgisLayer = Leaflet.tileLayer('http://a.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png');
  arcgisLayer.addTo(map);

  //we can also include vector data. geojson is a very popular format.
  var nbLayer, censusLayer;
  jquery
    .ajax({
      url: 'louisville_neighbourhoods.geojson'
    })
    .then(function(nbData) {
      //we can interact with vector data
      nbLayer = Leaflet.geoJson(nbData, {
        onEachFeature: function(feature, layer) {
          //popups are fully managed by leaflet
          layer.bindPopup(feature.properties.name);
        },
        //we can style our objects, using a style object
        style: {
          weight: 2,
          opacity: 1,
          color: 'purple',
          dashArray: '3',
          fillOpacity: 0.1,
          fillColor: 'rgb(122,122,12)'
        }
      });
      nbLayer.addTo(map);
    })
    .then(function() {
      //query new vector data
      return jquery.ajax({
        url: 'louisville_censustracts.geojson'
      })
    })
    .then(function(censusData) {

      //we can create a style function that

      function findOrdinals() {
        var sample = censusData.features[0];
        var allProps = Object.keys(sample.properties);
        return allProps.filter(function(prop) {
          return typeof sample.properties[prop] === 'number';
        });
      }

      function findMinMax(prop) {

        var min = Infinity;
        var max = -Infinity;
        censusData.features.forEach(function(feat) {
          min = Math.min(feat.properties[prop], min);
          max = Math.max(feat.properties[prop], max);
        });

        return {
          min: min,
          max: max
        };
      }

      function buildColorMapper(min, max, colorRamp) {
        var range = max - min;
        return function(value) {
          var index = (value - min) / range * colorRamp.length;
          index -= 1;
          index = Math.round(index);
          index = Math.max(0, index);
          index = Math.min(index, colorRamp.length - 1);
          return colorRamp[index];
        };

      }


      function makeStyleMapper(property) {
        var mima = findMinMax(property);
        var mapColor = buildColorMapper(mima.min, mima.max, ['#800026', '#BD0026', '#E31A1C', '#FC4E2A', '#FD8D3C', '#FEB24C', '#FED976', '#FFEDA0']);

        //our style function takes a feature, and we return a color on the color ramp.
        return function(feature) {
          return {
            fillColor: mapColor(feature.properties[property]),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
          };
        };
      }

      var selectForm = jquery('<select />');
      var ordinalFields = findOrdinals();
      ordinalFields.forEach(function(prop) {
        jquery('<option />', {value: prop, text: prop}).appendTo(selectForm);
      });
      selectForm.change(function(e) {
        var prop = selectForm.val();
        var stf = makeStyleMapper(prop);
        censusLayer.setStyle(stf);
      });
      jquery('#selectForm').append(selectForm);


      censusLayer = Leaflet.geoJson(censusData, {
        style: makeStyleMapper(selectForm.val())
      });
      censusLayer.addTo(map);
    })
    .then(function() {

      //use a layer control to turn on and off layers

      //base maps are mutually exclusive
      var baseMaps = {
        "Open Street Maps": osmLayer,
        "Bicycle Map": arcgisLayer
      };

      //overlay maps are not.
      var overlayMaps = {
        "Neighbourhoods": nbLayer,
        "Census Tracts": censusLayer
      };

      Leaflet.control.layers(baseMaps, overlayMaps).addTo(map);
    });

});