define([
  'Leaflet',
  'jquery'
], function(Leaflet, jquery) {

  //make a map, set the center
  var map = Leaflet.map('map');
  map.setView([38.2500, -85.7667], 13);

  //OSM data can be accessed as tiles (x/y/z)
  var osmLayer = Leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  osmLayer.addTo(map);

});